import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CountriesComponent } from './pages/countries/countries.component';
import { SearchCountriesComponent } from './components/search-countries/search-countries.component';
import { ListCountriesComponent } from './components/list-countries/list-countries.component';
import { CountryModalComponent } from './components/country-modal/country-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    CountriesComponent,
    SearchCountriesComponent,
    ListCountriesComponent,
    CountryModalComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
