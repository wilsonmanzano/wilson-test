import { Component, OnInit } from '@angular/core';
import { CountriesService } from '../../core/services/countries.service';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.scss']
})
export class CountriesComponent implements OnInit {

  constructor(private countriesService: CountriesService) { }

  ngOnInit(): void {
    this.getCountries();
  }

  getCountries() {
    this.countriesService.getCountries().subscribe(
      response => console.log(response),
      error => console.error(error)
    );
  }

}
